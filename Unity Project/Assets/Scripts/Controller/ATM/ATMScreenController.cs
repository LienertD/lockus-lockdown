﻿using UnityEngine;
using System.ComponentModel;
using App.Controller.Inventory;

namespace App.Controller.Location
{
    public class ATMScreenController : MonoBehaviour, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ATMScreens InEditorVisibleScreen = default;

        [SerializeField]
        private ATMScreens _startScreen = ATMScreens.NoCard;

        private InventoryController _inventoryController;
        private LocationController _locationController;

        private ATMScreens? _currentScreen;
        public ATMScreens CurrentScreen
        {
            get
            {
                return _currentScreen ?? default;
            }
            set
            {
                if (_currentScreen == value)
                {
                    return;
                }

                _currentScreen = value;
                OnPropertyChanged(nameof(CurrentScreen));
            }
        }

        private void Awake()
        {
            _locationController = GetComponentInParent<LocationController>();
            _inventoryController = GetComponentInParent<InventoryController>();

            CurrentScreen = _startScreen;
        }

        private void OnEnable()
        {
            _inventoryController.ItemsInInventory.CollectionChanged += ItemsInInventory_CollectionChanged;
        }

        private void OnDisable()
        {
            _inventoryController.ItemsInInventory.CollectionChanged -= ItemsInInventory_CollectionChanged;
        }

        private void ItemsInInventory_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (_locationController.CurrentLocation == Locations.ATM)
            {
                if (e.OldItems != null && e.OldItems.Contains(InventoryItems.BankCard))
                {
                    CurrentScreen = ATMScreens.LoggedOut;
                }
            }

            if (e.NewItems != null && e.NewItems.Contains(InventoryItems.BankCard))
            {
                CurrentScreen = ATMScreens.NoCard;
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum ATMScreens
    {
        NoCard = 100,
        LoggedOut = 200,
        LoggedIn = 300,
    }
}
