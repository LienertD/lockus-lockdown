﻿#if !UNITY_EDITOR
using System.Runtime.InteropServices;
#endif

namespace App.Controller
{
    public static class OpenHyperlinkHelper
    {
        public static void OpenLinkInNewTab(string link)
        {
#if !UNITY_EDITOR
            openWindow(link);
#endif
        }

#if !UNITY_EDITOR
        [DllImport("__Internal")]
        private static extern void openWindow(string url);
#endif
    }
}
