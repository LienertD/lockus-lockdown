﻿using UnityEngine;
using System.ComponentModel;

namespace App.Controller.Calendar
{
    public class CalendarLockController : MonoBehaviour
    {
        public LockStates InEditorVisibleLockState = default;

        [SerializeField]
        private LockStates _startLockState = LockStates.Locked;

        public event PropertyChangedEventHandler PropertyChanged;

        public enum LockStates
        {
            Locked,
            Unlocked
        }

        private LockStates? _currentLockState;
        public LockStates CurrentLockState
        {
            get
            {
                return _currentLockState ?? default;
            }
            set
            {
                if (_currentLockState == value)
                {
                    return;
                }

                _currentLockState = value;
                OnPropertyChanged(nameof(CurrentLockState));
            }
        }

        private void Awake()
        {
            CurrentLockState = _startLockState;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
