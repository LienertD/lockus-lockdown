﻿using UnityEngine;
using App.Controller.Location;
using System.Collections.Generic;
using App.Controller.Calendar;

namespace App.Controller.Hints
{
    public class HintController : MonoBehaviour
    {
        private const string _baseHintGroupsLink = "https://bigpursuit.be/hunt/overview.php?gameRequest=hint&gameCode=LUBOM85&hintGroup=";
        private List<Locations> _visitedLocations = new List<Locations>();
        private LocationController _locationController;
        private CalendarLockController _calendarLockController;

        private const string NotYetInsdeGroupId = "not_yet_inside";
        private const string DidNotFoundWalletYetGroupId = "did_not_found_wallet_yet";
        private const string DidNotFoundLockerYetGroupId = "did_not_found_locker_yet";
        private const string DidNotOpenLockerYetGroupId = "did_not_open_locker_yet";
        private const string DidNotInputCorrectAMTCodeYetGroupId = "did_not_input_correct_atm_code_yet";

        public List<HintGroup> _hintGroups = new List<HintGroup>
        {
            new HintGroup
            {
                LinkSuffix = "10",
                ID = NotYetInsdeGroupId,
                Order = 0,
            },
            new HintGroup
            {
                LinkSuffix = "20",
                ID = DidNotFoundWalletYetGroupId,
                Order = 1,
            },
            new HintGroup
            {
                LinkSuffix = "30",
                ID = DidNotFoundLockerYetGroupId,
                Order = 2,
            },
            new HintGroup
            {
                LinkSuffix = "40",
                ID = DidNotOpenLockerYetGroupId,
                Order = 3,
            },
            new HintGroup
            {
                LinkSuffix = "50",
                ID = DidNotInputCorrectAMTCodeYetGroupId,
                Order = 5,
            },
        };

        private HintGroup _currentHintGroup;
        private HintGroup CurrentHintGroup
        {
            get
            {
                return _currentHintGroup;
            }
            set
            {
                if (_currentHintGroup == value)
                {
                    return;
                }

                _currentHintGroup = value;
            }
        }

        private void Awake()
        {
            _locationController = GetComponent<LocationController>();
            _calendarLockController = GetComponentInChildren<CalendarLockController>();
        }

        private void OnEnable()
        {
            _locationController.PropertyChanged += LocationController_PropertyChanged;
            _calendarLockController.PropertyChanged += CalendarLockController_PropertyChanged;
        }

        private void OnDisable()
        {
            _locationController.PropertyChanged -= LocationController_PropertyChanged;
            _calendarLockController.PropertyChanged -= CalendarLockController_PropertyChanged;
        }

        private void LocationController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LocationController.CurrentLocation))
            {
                OnCurrentLocationChanged();
            }
        }

        private void CalendarLockController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CalendarLockController.CurrentLockState))
            {
                OnCalenderLockStateChanged();
            }
        }

        private void UnlockHintGroup(string id)
        {
            var newGroup = _hintGroups.Find(h => h.ID == id);
            if (newGroup == null)
            {
                return;
            }

            CurrentHintGroup = newGroup;
        }

        public string GetCurrentHintLink()
        {
            if (CurrentHintGroup == null)
            {
                return null;
            }

            return $"{_baseHintGroupsLink}{CurrentHintGroup.LinkSuffix}";
        }

        private void OnCurrentLocationChanged()
        {
            if (!_visitedLocations.Contains(_locationController.CurrentLocation))
            {
                _visitedLocations.Add(_locationController.CurrentLocation);
            }

            switch (_locationController.CurrentLocation)
            {
                case Locations.OutsideOverview:
                    {
                        UnlockHintGroup(NotYetInsdeGroupId);
                        break;
                    }
                case Locations.InsideOverview:
                    {
                        if (!_visitedLocations.Contains(Locations.Calender) && !_visitedLocations.Contains(Locations.Wallet))
                        {
                            UnlockHintGroup(DidNotFoundLockerYetGroupId);
                        }
                        break;
                    }
                case Locations.Calender:
                    {
                        if (!_visitedLocations.Contains(Locations.Wallet))
                        {
                            UnlockHintGroup(DidNotFoundWalletYetGroupId);
                        }
                        else
                        {
                            OnCalendarOrWalletVisited();
                        }
                        break;
                    }
                case Locations.Wallet:
                    {
                        if (!_visitedLocations.Contains(Locations.Calender))
                        {
                            UnlockHintGroup(DidNotFoundLockerYetGroupId);
                        }
                        else
                        {
                            OnCalendarOrWalletVisited();
                        }
                        break;
                    }
            }
        }

        private void OnCalenderLockStateChanged()
        {
            if (_calendarLockController.CurrentLockState == CalendarLockController.LockStates.Unlocked)
            {
                UnlockHintGroup(DidNotInputCorrectAMTCodeYetGroupId);
            }
        }

        private void OnCalendarOrWalletVisited()
        {
            if (_visitedLocations.Contains(Locations.Wallet) && _visitedLocations.Contains(Locations.Calender))
            {
                if (_calendarLockController.CurrentLockState == CalendarLockController.LockStates.Locked)
                {
                    UnlockHintGroup(DidNotOpenLockerYetGroupId);
                }
            }
        }
    }

    public class HintGroup
    {
        public string LinkSuffix { get; set; }
        public string ID { get; set; }
        public int Order { get; set; }
    }
}
