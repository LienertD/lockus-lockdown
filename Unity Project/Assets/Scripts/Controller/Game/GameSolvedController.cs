﻿using System.ComponentModel;
using UnityEngine;

namespace App.Controller.Game
{
    public class GameSolvedController : MonoBehaviour
    {
        public bool InEditorVisibleIsSolved = default;
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isSolved = false;
        public bool IsSolved
        {
            get
            {
                return _isSolved;
            }
            set
            {
                if (_isSolved == value)
                {
                    return;
                }

                _isSolved = value;
                OnPropertyChanged(nameof(IsSolved));
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

