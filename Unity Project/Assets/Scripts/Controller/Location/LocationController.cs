﻿using UnityEngine;
using System.ComponentModel;

namespace App.Controller.Location
{
    public class LocationController : MonoBehaviour, INotifyPropertyChanged
    {
        [SerializeField]
        private Locations _startLocation = Locations.IntroScreen;

        public Locations InEditorVisibleLocation = default;

        public event PropertyChangedEventHandler PropertyChanged;

        private Locations? _currentLocation;
        public Locations CurrentLocation
        {
            get
            {
                return _currentLocation ?? default;
            }
            set
            {
                if (_currentLocation == value)
                {
                    return;
                }

                _currentLocation = value;
                OnPropertyChanged(nameof(CurrentLocation));
            }
        }

        private void Awake()
        {
            CurrentLocation = _startLocation;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum Locations
    {
        IntroScreen = 100,
        OutsideOverview = 200,
        Scanner = 400,
        InsideOverview = 500,
        Wallet = 550,
        Calender = 600,
        ATM = 700,
    }
}
