﻿using UnityEngine;
using System.Collections.ObjectModel;

namespace App.Controller.Inventory
{
    public enum InventoryItems
    {
        BankCard = 100,
    }

    public class InventoryController : MonoBehaviour
    {
        public ObservableCollection<InventoryItems> ItemsInInventory
        {
            get; set;
        } = new ObservableCollection<InventoryItems>();
    }
}
