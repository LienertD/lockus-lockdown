﻿using UnityEngine;
using App.Controller.Inventory;

namespace App.View.Inventory
{
    public class EnableMonobehaviourOnItemInInventory : MonoBehaviour
    {
        [SerializeField]
        private InventoryItems _item = default;

        [SerializeField]
        private MonoBehaviour _monoBehaviour = default;

        [SerializeField]
        private bool _invert = default;

        private InventoryController _inventoryController;

        private void Awake()
        {
            _inventoryController = GetComponentInParent<InventoryController>();
        }

        private void OnEnable()
        {
            _inventoryController.ItemsInInventory.CollectionChanged += OnItemsInInventoryChanged;
        }

        private void Start()
        {
            SetEnabled();
        }

        private void OnDisable()
        {
            _inventoryController.ItemsInInventory.CollectionChanged -= OnItemsInInventoryChanged;
        }

        private void OnItemsInInventoryChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            SetEnabled();
        }

        private void SetEnabled()
        {
            _monoBehaviour.enabled = _inventoryController.ItemsInInventory.Contains(_item) ^ _invert;
        }
    }
}
