﻿using UnityEngine;
#if !UNITY_ENGINE
using App.Controller;
#endif
using UnityEngine.EventSystems;

namespace App.View
{
    public class OpenLinkOnClick : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField]
        private string _linkToOpen = default;

        public void OnPointerDown(PointerEventData eventData)
        {
#if !UNITY_EDITOR
            OpenHyperlinkHelper.OpenLinkInNewTab(_linkToOpen);
#else
            Application.OpenURL(_linkToOpen);
#endif
        }
    }
}
