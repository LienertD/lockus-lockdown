﻿#if !UNITY_EDITOR
using App.Controller;
#endif
using App.Controller.Hints;
using UnityEngine;
using UnityEngine.EventSystems;

namespace App.View.Hints
{
    public class ShowHintButton : MonoBehaviour, IPointerDownHandler
    {
        private HintController _hintController;

        private void Awake()
        {
            _hintController = GetComponentInParent<HintController>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            string hintLink = _hintController.GetCurrentHintLink();
            if (hintLink == null)
            {
                return;
            }

#if !UNITY_EDITOR
            OpenHyperlinkHelper.OpenLinkInNewTab(hintLink);
#else
            Application.OpenURL(hintLink);
#endif
        }
    }
}
