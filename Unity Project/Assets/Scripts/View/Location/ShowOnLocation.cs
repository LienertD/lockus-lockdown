﻿using UnityEngine;
using System.Collections;
using App.Controller.Location;

namespace App.View.Location
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasGroup))]
    public class ShowOnLocation : MonoBehaviour
    {
        [SerializeField]
        private Locations _showOnLocation = default;

        [SerializeField]
        private float _transitionDuration = 0f;

        [SerializeField]
        private bool _invert = false;

        private LocationController _locationController;
        private CanvasGroup _canvasGroup;
        private IEnumerator _transitionRoutine = null;
        public Locations? _previousInEditorVisibleLocation = null;

        private void Awake()
        {
            _locationController = GetComponentInParent<LocationController>();
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        private void OnEnable()
        {
            _locationController.PropertyChanged += LocationController_PropertyChanged;
        }

        private void Start()
        {
            // Snap to start state
            StartTransitionRoutine(CanShow(), 0);
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                return;
            }

            if (_locationController.InEditorVisibleLocation != _previousInEditorVisibleLocation)
            {
                StartTransitionRoutine(CanShow(_locationController.InEditorVisibleLocation), 0);
                _previousInEditorVisibleLocation = _locationController.InEditorVisibleLocation;
            }
#endif
        }

        private void OnDisable()
        {
            _locationController.PropertyChanged -= LocationController_PropertyChanged;
        }

        private bool CanShow(Locations? currentLocationToCompareTo = null)
        {
            if (currentLocationToCompareTo == null)
            {
                currentLocationToCompareTo = _locationController.CurrentLocation;
            }

            return currentLocationToCompareTo == _showOnLocation ^ _invert;
        }

        private void LocationController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LocationController.CurrentLocation))
            {
                OnCurrentLocationChanged();
            }
        }

        private void OnCurrentLocationChanged()
        {
            StartTransitionRoutine(CanShow(), _transitionDuration);
        }

        private void StartTransitionRoutine(bool show, float duration)
        {
            StopTransitionRoutine();
            _transitionRoutine = RunTransitionRoutine(show, duration);
            StartCoroutine(_transitionRoutine);
        }

        private IEnumerator RunTransitionRoutine(bool show, float duration)
        {
            // Block click catching while transitioning
            _canvasGroup.blocksRaycasts = false;

            float startAlpha = _canvasGroup.alpha;
            float destinationAlpha = show ? 1f : 0f;

            float elapsedTransitionTime = 0f;

            if (duration > 0 && startAlpha != destinationAlpha)
            {
                while (elapsedTransitionTime < duration)
                {
                    _canvasGroup.alpha = Mathf.Lerp(startAlpha, destinationAlpha, elapsedTransitionTime / duration);

                    yield return null;
                    elapsedTransitionTime += Time.deltaTime;
                }
            }

            _canvasGroup.alpha = destinationAlpha;

            if (show)
            {
                _canvasGroup.blocksRaycasts = true;
            }

            _transitionRoutine = null;
        }

        private void StopTransitionRoutine()
        {
            if (_transitionRoutine != null)
            {
                StopCoroutine(_transitionRoutine);
                _transitionRoutine = null;
            }
        }
    }
}
