﻿using UnityEngine;
using UnityEngine.UI;
using App.Controller.Location;

namespace App.View.Location
{
    [RequireComponent(typeof(Button))]
    public class GoToLocationOnClick : MonoBehaviour
    {
        [SerializeField]
        private Locations _goToLocation = default;

        private Button _button;
        private LocationController _locationController;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _locationController = GetComponentInParent<LocationController>();
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(() => OnButtonClick());
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(() => OnButtonClick());
        }

        private void OnButtonClick()
        {
            _locationController.CurrentLocation = _goToLocation;
        }
    }
}
