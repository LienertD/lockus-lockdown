﻿using UnityEngine;
using System.Collections;
using App.Controller.Location;

namespace App.View.Location
{
    public class GoToLocationAfterDelay : MonoBehaviour
    {
        [SerializeField]
        private Locations _onLocation = default;

        [SerializeField]
        private Locations _goToLocation = default;

        [SerializeField]
        private float _delay = default;

        private LocationController _locationController;
        private IEnumerator _delayedLocationNavigationRoutine = null;

        private void Awake()
        {
            _locationController = GetComponentInParent<LocationController>();
        }

        private void OnEnable()
        {
            _locationController.PropertyChanged += LocationController_PropertyChanged;
        }

        private void OnDisable()
        {
            _locationController.PropertyChanged -= LocationController_PropertyChanged;
        }

        private void LocationController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LocationController.CurrentLocation))
            {
                OnCurrentLocationChanged();
            }
        }

        private void OnCurrentLocationChanged()
        {
            if (_locationController.CurrentLocation == _onLocation)
            {
                StartDelayedLocationNavigationRoutine();
            }
            else
            {
                StopDelayedLocationNavigationRoutine();
            }
        }

        private void StartDelayedLocationNavigationRoutine()
        {
            StopDelayedLocationNavigationRoutine();
            _delayedLocationNavigationRoutine = RunDelayedLocationNavigationRoutine();
            StartCoroutine(_delayedLocationNavigationRoutine);
        }

        private IEnumerator RunDelayedLocationNavigationRoutine()
        {
            yield return new WaitForSeconds(_delay);
            _locationController.CurrentLocation = _goToLocation;

            _delayedLocationNavigationRoutine = null;
        }

        private void StopDelayedLocationNavigationRoutine()
        {
            if (_delayedLocationNavigationRoutine != null)
            {
                StopCoroutine(_delayedLocationNavigationRoutine);
                _delayedLocationNavigationRoutine = null;
            }
        }
    }
}
