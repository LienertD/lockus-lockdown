﻿using UnityEngine;
using UnityEngine.UI;

namespace App.View.ATM
{
    [RequireComponent(typeof(Button))]
    public class ATMCodeKeypadButton : MonoBehaviour
    {
        public enum Types
        {
            Key,
            Stop,
            Correction,
            OK
        }

        [SerializeField]
        private Types _type = default;

        [SerializeField]
        private string _key = default;

        private Button _button;
        private ATMCodeKeypad _ATMCodeKeypad;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _ATMCodeKeypad = GetComponentInParent<ATMCodeKeypad>();
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(() => OnButtonClick());
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(() => OnButtonClick());
        }

        private void OnButtonClick()
        {
            switch (_type)
            {
                case Types.Key:
                    {
                        _ATMCodeKeypad.OnCodeKeyNumberClicked(_key);
                        break;
                    }
                case Types.Stop:
                    {
                        _ATMCodeKeypad.OnStopCodeKeyClicked();
                        break;
                    }
                case Types.Correction:
                    {
                        _ATMCodeKeypad.OnCorrectionCodeKeyClicked();
                        break;
                    }
                case Types.OK:
                    {
                        _ATMCodeKeypad.OnOKCodeKeyClicked();
                        break;
                    }
            }
        }
    }
}
