﻿using UnityEngine;
using App.Controller.Location;
using System.Collections.Generic;

namespace App.View.ATM
{
    public class ATMLoggedOutView : MonoBehaviour
    {
        [SerializeField]
        private ATMCodeKeypad _ATMCodeKeypad = default;

        [SerializeField]
        private List<ScreenCodeNumberDisplay> _screenCodeNumberDisplays = default;

        private ATMScreenController _ATMScreenController;
        private LocationController _locationController;
        private string _correctCode = "4956";

        private string _currentCode = string.Empty;
        private string CurrentCode
        {
            get
            {
                return _currentCode;
            }
            set
            {
                if (value == _currentCode)
                {
                    return;
                }

                _currentCode = value;
                OnCurrentCodeChanged();
            }
        }

        private void Awake()
        {
            _ATMScreenController = GetComponentInParent<ATMScreenController>();
            _locationController = GetComponentInParent<LocationController>();
        }

        private void OnEnable()
        {
            _ATMScreenController.PropertyChanged += ATMScreenController_PropertyChanged;

            _ATMCodeKeypad.OnCodeKeyNumberClickedEvent += ATMCodeKeypad_OnCodeKeyNumberClickedEvent;
            _ATMCodeKeypad.OnCorrectionCodeKeyClickedEvent += ATMCodeKeypad_OnCorrectionCodeKeyClickedEvent;
            _ATMCodeKeypad.OnStopCodeKeyClickedEvent += ATMCodeKeypad_OnStopCodeKeyClickedEvent;
            _ATMCodeKeypad.OnOKCodeKeyClickedEvent += ATMCodeKeypad_OnOKCodeKeyClickedEvent;
        }

        private void Start()
        {
            ClearCode();
        }

        private void OnDisable()
        {
            _ATMScreenController.PropertyChanged -= ATMScreenController_PropertyChanged;

            _ATMCodeKeypad.OnCodeKeyNumberClickedEvent -= ATMCodeKeypad_OnCodeKeyNumberClickedEvent;
            _ATMCodeKeypad.OnCorrectionCodeKeyClickedEvent -= ATMCodeKeypad_OnCorrectionCodeKeyClickedEvent;
            _ATMCodeKeypad.OnStopCodeKeyClickedEvent -= ATMCodeKeypad_OnStopCodeKeyClickedEvent;
            _ATMCodeKeypad.OnOKCodeKeyClickedEvent -= ATMCodeKeypad_OnOKCodeKeyClickedEvent;
        }

        private void ATMScreenController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ATMScreenController.CurrentScreen))
            {
                if (_ATMScreenController.CurrentScreen == ATMScreens.NoCard)
                {
                    ClearCode();
                }
            }
        }

        private void ATMCodeKeypad_OnCodeKeyNumberClickedEvent(string key)
        {
            if (_locationController.CurrentLocation != Locations.ATM || _ATMScreenController.CurrentScreen != ATMScreens.LoggedOut)
            {
                return;
            }

            if (CurrentCode.Length >= _correctCode.Length)
            {
                ClearCode();
            }

            CurrentCode += key;

            // Display entered number
            _screenCodeNumberDisplays[Mathf.Min(CurrentCode.Length - 1, _screenCodeNumberDisplays.Count - 1)].ShowNumber(key);
        }

        private void ATMCodeKeypad_OnOKCodeKeyClickedEvent(object sender, System.EventArgs e)
        {
            if (CurrentCode.Length == _correctCode.Length && CurrentCode != _correctCode)
            {
                ClearCode();
            }
        }

        private void ATMCodeKeypad_OnStopCodeKeyClickedEvent(object sender, System.EventArgs e)
        {
            ClearCode();
        }

        private void ATMCodeKeypad_OnCorrectionCodeKeyClickedEvent(object sender, System.EventArgs e)
        {
            if (CurrentCode.Length >= 1)
            {
                _screenCodeNumberDisplays[CurrentCode.Length - 1].Clear();
                CurrentCode = CurrentCode.Remove(CurrentCode.Length - 1);
            }
        }

        private void ClearCode()
        {
            CurrentCode = string.Empty;
        }

        private void OnCurrentCodeChanged()
        {
            if (CurrentCode == string.Empty)
            {
                foreach (var display in _screenCodeNumberDisplays)
                {
                    display.Clear();
                }
            }

            // Check if code is correct
            if (CurrentCode == _correctCode)
            {
                _ATMScreenController.CurrentScreen = ATMScreens.LoggedIn;
            }
        }
    }
}
