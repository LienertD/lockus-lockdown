﻿using System;
using TMPro;
using UnityEngine;
using App.Controller.Location;

namespace App.View.ATM
{
    public class ATMDateTimeDisplay : MonoBehaviour
    {
        private TMP_Text _text;
        private ATMScreenController _ATMScreenController;
        private LocationController _locationController;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
            _ATMScreenController = GetComponentInParent<ATMScreenController>();
            _locationController = GetComponentInParent<LocationController>();
        }

        private void Update()
        {
            if (_locationController.CurrentLocation != Locations.ATM || _ATMScreenController.CurrentScreen != ATMScreens.NoCard)
            {
                return;
            }

            _text.text = $"15/01/2020<br>{DateTime.Now:HH:mm}";
        }
    }
}
