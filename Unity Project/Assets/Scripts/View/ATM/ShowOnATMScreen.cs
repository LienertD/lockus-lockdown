﻿using UnityEngine;
using System.Collections;
using App.Controller.Location;

namespace App.View.ATM
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasGroup))]
    public class ShowOnATMScreen : MonoBehaviour
    {
        [SerializeField]
        private ATMScreens _showOnScreen = default;

        [SerializeField]
        private float _transitionDuration = 0f;

        private ATMScreenController _ATMScreenController;
        private CanvasGroup _canvasGroup;
        private IEnumerator _transitionRoutine = null;
        public ATMScreens? _previousInEditorVisibleScreen = null;

        private void Awake()
        {
            _ATMScreenController = GetComponentInParent<ATMScreenController>();
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        private void OnEnable()
        {
            _ATMScreenController.PropertyChanged += ATMScreenController_PropertyChanged;
        }

        private void Start()
        {
            // Snap to start state
            StartTransitionRoutine(_ATMScreenController.CurrentScreen == _showOnScreen, 0);
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                return;
            }

            if (_ATMScreenController.InEditorVisibleScreen != _previousInEditorVisibleScreen)
            {
                StartTransitionRoutine(_ATMScreenController.InEditorVisibleScreen == _showOnScreen, 0);
                _previousInEditorVisibleScreen = _ATMScreenController.InEditorVisibleScreen;
            }
#endif
        }

        private void OnDisable()
        {
            _ATMScreenController.PropertyChanged -= ATMScreenController_PropertyChanged;
        }

        private void ATMScreenController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ATMScreenController.CurrentScreen))
            {
                OnCurrentScreenChanged();
            }
        }

        private void OnCurrentScreenChanged()
        {
            StartTransitionRoutine(_ATMScreenController.CurrentScreen == _showOnScreen, _transitionDuration);
        }

        private void StartTransitionRoutine(bool show, float duration)
        {
            StopTransitionRoutine();
            _transitionRoutine = RunTransitionRoutine(show, duration);
            StartCoroutine(_transitionRoutine);
        }

        private IEnumerator RunTransitionRoutine(bool show, float duration)
        {
            // Block click catching while transitioning
            _canvasGroup.blocksRaycasts = false;

            float startAlpha = _canvasGroup.alpha;
            float destinationAlpha = show ? 1f : 0f;

            float elapsedTransitionTime = 0f;

            if (duration > 0 && startAlpha != destinationAlpha)
            {
                while (elapsedTransitionTime < duration)
                {
                    _canvasGroup.alpha = Mathf.Lerp(startAlpha, destinationAlpha, elapsedTransitionTime / duration);

                    yield return null;
                    elapsedTransitionTime += Time.deltaTime;
                }
            }

            _canvasGroup.alpha = destinationAlpha;

            if (show)
            {
                _canvasGroup.blocksRaycasts = true;
            }

            _transitionRoutine = null;
        }

        private void StopTransitionRoutine()
        {
            if (_transitionRoutine != null)
            {
                StopCoroutine(_transitionRoutine);
                _transitionRoutine = null;
            }
        }
    }
}
