﻿using System;
using UnityEngine;

namespace App.View.ATM
{
    public class ATMCodeKeypad : MonoBehaviour
    {
        public delegate void OnCodeKeyPressedDelegate(string key);
        public event OnCodeKeyPressedDelegate OnCodeKeyNumberClickedEvent;
        public event EventHandler OnStopCodeKeyClickedEvent;
        public event EventHandler OnOKCodeKeyClickedEvent;
        public event EventHandler OnCorrectionCodeKeyClickedEvent;

        public void OnCodeKeyNumberClicked(string number)
        {
            OnCodeKeyNumberClickedEvent?.Invoke(number);
        }

        public void OnStopCodeKeyClicked()
        {
            OnStopCodeKeyClickedEvent?.Invoke(this, null);
        }

        public void OnOKCodeKeyClicked()
        {
            OnOKCodeKeyClickedEvent?.Invoke(this, null);
        }

        public void OnCorrectionCodeKeyClicked()
        {
            OnCorrectionCodeKeyClickedEvent?.Invoke(this, null);
        }
    }
}
