﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace App.View.ATM
{
    public class ScreenCodeNumberDisplay : MonoBehaviour
    {
        private TMP_Text _text;
        private IEnumerator _makeNumberForSomeTimeRoutine = null;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
        }

        private void Start()
        {
            Clear();
        }

        public void ShowNumber(string number)
        {
            StartMakeNumberForSomeTimeRoutine(number);
        }

        public void Clear()
        {
            StopMakeNumberForSomeTimeRoutine();
            _text.text = "•";
        }

        private void StartMakeNumberForSomeTimeRoutine(string number)
        {
            StopMakeNumberForSomeTimeRoutine();
            _makeNumberForSomeTimeRoutine = RunMakeNumberForSomeTimeRoutine(number);
            StartCoroutine(_makeNumberForSomeTimeRoutine);
        }

        private IEnumerator RunMakeNumberForSomeTimeRoutine(string number)
        {
            _text.text = number;
            yield return new WaitForSeconds(0.25f); // Show the number for some time
            _text.text = "*";

            _makeNumberForSomeTimeRoutine = null;
        }

        private void StopMakeNumberForSomeTimeRoutine()
        {
            if (_makeNumberForSomeTimeRoutine != null)
            {
                StopCoroutine(_makeNumberForSomeTimeRoutine);
                _makeNumberForSomeTimeRoutine = null;
            }
        }
    }
}
