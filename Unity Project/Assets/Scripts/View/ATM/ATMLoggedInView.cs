﻿using UnityEngine;
using UnityEngine.UI;
using App.Controller.Location;
using App.Controller.Game;

namespace App.View.ATM
{
    public class ATMLoggedInView : MonoBehaviour
    {
        [SerializeField]
        private Button _withdrawButton = default;

        private ATMScreenController _ATMScreenController;
        private LocationController _locationController;
        private GameSolvedController _gameSolvedController;

        private void Awake()
        {
            _ATMScreenController = GetComponentInParent<ATMScreenController>();
            _locationController = GetComponentInParent<LocationController>();
            _gameSolvedController = GetComponentInParent<GameSolvedController>();
        }

        private void OnEnable()
        {
            _withdrawButton.onClick.AddListener(() => OnWithdrawButtonClick());
        }

        private void OnDisable()
        {
            _withdrawButton.onClick.RemoveListener(() => OnWithdrawButtonClick());
        }

        private void OnWithdrawButtonClick()
        {
            if (_locationController.CurrentLocation != Locations.ATM || _ATMScreenController.CurrentScreen != ATMScreens.LoggedIn)
            {
                return;
            }

            // Mark game as solved code
            _gameSolvedController.IsSolved = true;
        }
    }
}
