﻿using UnityEngine;
using System.Collections;
using App.Controller.Game;

namespace App.View.Game
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasGroup))]
    public class ShowOnGameSolved : MonoBehaviour
    {
        [SerializeField]
        private bool _showOnIsSolved = true;

        [SerializeField]
        private float _transitionDuration = 0f;

        private GameSolvedController _gameSolvedController;
        private CanvasGroup _canvasGroup;
        private IEnumerator _transitionRoutine = null;
        public bool? _previousInEditorVisibleIsSolved = null;

        private void Awake()
        {
            _gameSolvedController = GetComponentInParent<GameSolvedController>();
            if (_gameSolvedController == null)
            {
                _gameSolvedController = FindObjectOfType<GameSolvedController>();
            }

            _canvasGroup = GetComponent<CanvasGroup>();
        }

        private void OnEnable()
        {
            _gameSolvedController.PropertyChanged += GameSolvedController_PropertyChanged;
        }

        private void Start()
        {
            // Snap to start state
            StartTransitionRoutine(_gameSolvedController.IsSolved == _showOnIsSolved, 0);
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                return;
            }

            if (_gameSolvedController.InEditorVisibleIsSolved != _previousInEditorVisibleIsSolved)
            {
                StartTransitionRoutine(_gameSolvedController.InEditorVisibleIsSolved == _showOnIsSolved, 0);
                _previousInEditorVisibleIsSolved = _gameSolvedController.InEditorVisibleIsSolved;
            }
#endif
        }

        private void OnDisable()
        {
            _gameSolvedController.PropertyChanged -= GameSolvedController_PropertyChanged;
        }

        private void GameSolvedController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(GameSolvedController.IsSolved))
            {
                OnCurrentScreenChanged();
            }
        }

        private void OnCurrentScreenChanged()
        {
            StartTransitionRoutine(_gameSolvedController.IsSolved == _showOnIsSolved, _transitionDuration);
        }

        private void StartTransitionRoutine(bool show, float duration)
        {
            StopTransitionRoutine();
            _transitionRoutine = RunTransitionRoutine(show, duration);
            StartCoroutine(_transitionRoutine);
        }

        private IEnumerator RunTransitionRoutine(bool show, float duration)
        {
            // Block click catching while transitioning
            _canvasGroup.blocksRaycasts = false;

            float startAlpha = _canvasGroup.alpha;
            float destinationAlpha = show ? 1f : 0f;

            float elapsedTransitionTime = 0f;

            if (duration > 0 && startAlpha != destinationAlpha)
            {
                while (elapsedTransitionTime < duration)
                {
                    _canvasGroup.alpha = Mathf.Lerp(startAlpha, destinationAlpha, elapsedTransitionTime / duration);

                    yield return null;
                    elapsedTransitionTime += Time.deltaTime;
                }
            }

            _canvasGroup.alpha = destinationAlpha;

            if (show)
            {
                _canvasGroup.blocksRaycasts = true;
            }

            _transitionRoutine = null;
        }

        private void StopTransitionRoutine()
        {
            if (_transitionRoutine != null)
            {
                StopCoroutine(_transitionRoutine);
                _transitionRoutine = null;
            }
        }
    }
}
