﻿using System.Collections;
using App.Controller.Calendar;
using UnityEngine;

namespace App.View.Calendar
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasGroup))]
    public class ShowOnCalendarLockState : MonoBehaviour
    {
        [SerializeField]
        private CalendarLockController.LockStates _showOnLockState = default;

        [SerializeField]
        private float _transitionDuration = 0f;

        private CalendarLockController _calendarLockController;
        private CanvasGroup _canvasGroup;
        private IEnumerator _transitionRoutine = null;
        public CalendarLockController.LockStates? _previousInEditorVisibleLockScreen = null;

        private void Awake()
        {
            _calendarLockController = GetComponentInParent<CalendarLockController>();
            if (_calendarLockController == null)
            {
                _calendarLockController = FindObjectOfType<CalendarLockController>();
            }

            _canvasGroup = GetComponent<CanvasGroup>();
        }

        private void OnEnable()
        {
            _calendarLockController.PropertyChanged += CalendarLockController_PropertyChanged;
        }

        private void Start()
        {
            // Snap to start state
            StartTransitionRoutine(_calendarLockController.CurrentLockState == _showOnLockState, 0);
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                return;
            }

            if (_calendarLockController.InEditorVisibleLockState != _previousInEditorVisibleLockScreen)
            {
                StartTransitionRoutine(_calendarLockController.InEditorVisibleLockState == _showOnLockState, 0);
                _previousInEditorVisibleLockScreen = _calendarLockController.InEditorVisibleLockState;
            }
#endif
        }

        private void OnDisable()
        {
            _calendarLockController.PropertyChanged -= CalendarLockController_PropertyChanged;
        }

        private void CalendarLockController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CalendarLockController.CurrentLockState))
            {
                OnCurrentScreenChanged();
            }
        }

        private void OnCurrentScreenChanged()
        {
            StartTransitionRoutine(_calendarLockController.CurrentLockState == _showOnLockState, _transitionDuration);
        }

        private void StartTransitionRoutine(bool show, float duration)
        {
            StopTransitionRoutine();
            _transitionRoutine = RunTransitionRoutine(show, duration);
            StartCoroutine(_transitionRoutine);
        }

        private IEnumerator RunTransitionRoutine(bool show, float duration)
        {
            // Block click catching while transitioning
            _canvasGroup.blocksRaycasts = false;

            float startAlpha = _canvasGroup.alpha;
            float destinationAlpha = show ? 1f : 0f;

            float elapsedTransitionTime = 0f;

            if (duration > 0 && startAlpha != destinationAlpha)
            {
                while (elapsedTransitionTime < duration)
                {
                    _canvasGroup.alpha = Mathf.Lerp(startAlpha, destinationAlpha, elapsedTransitionTime / duration);

                    yield return null;
                    elapsedTransitionTime += Time.deltaTime;
                }
            }

            _canvasGroup.alpha = destinationAlpha;

            if (show)
            {
                _canvasGroup.blocksRaycasts = true;
            }

            _transitionRoutine = null;
        }

        private void StopTransitionRoutine()
        {
            if (_transitionRoutine != null)
            {
                StopCoroutine(_transitionRoutine);
                _transitionRoutine = null;
            }
        }
    }
}
