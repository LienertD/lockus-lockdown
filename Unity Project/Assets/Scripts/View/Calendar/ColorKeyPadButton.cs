﻿using UnityEngine;
using UnityEngine.UI;

namespace App.View.Calendar
{
    [RequireComponent(typeof(Button))]
    public class ColorKeyPadButton : MonoBehaviour
    {
        [SerializeField]
        private string _colorKey = default;

        private Button _button;
        private ColorKeyPad _colorKeyPad;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _colorKeyPad = GetComponentInParent<ColorKeyPad>();
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(() => OnButtonClick());
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(() => OnButtonClick());
        }

        private void OnButtonClick()
        {
            _colorKeyPad.OnColorKeyPressed(_colorKey);
        }
    }
}
