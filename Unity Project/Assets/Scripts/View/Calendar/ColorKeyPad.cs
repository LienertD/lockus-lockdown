﻿using UnityEngine;
using UnityEngine.UI;
using App.Controller.Calendar;
using System.Collections.Generic;

namespace App.View.Calendar
{
    public class ColorKeyPad : MonoBehaviour
    {
        [SerializeField]
        private Image _feedbackLightImage = default;

        [SerializeField]
        private List<string> _correctCombination = default;

        private List<string> _currentCombination = new List<string>();

        private CalendarLockController _calendarLockController;

        private void Awake()
        {
            _calendarLockController = GetComponentInParent<CalendarLockController>();
        }

        public void OnColorKeyPressed(string colorCode)
        {
            _currentCombination.Add(colorCode);

            SetFeedbackLightColor(colorCode);

            if (_currentCombination.Count > _correctCombination.Count)
            {
                _currentCombination.RemoveRange(0, _currentCombination.Count - _correctCombination.Count);
            }

            if (AreCombinationEqual(_currentCombination, _correctCombination))
            {
                _calendarLockController.CurrentLockState = CalendarLockController.LockStates.Unlocked;
            }
        }

        private void SetFeedbackLightColor(string colorCode)
        {
            if (ColorUtility.TryParseHtmlString($"#{colorCode}", out Color color))
            {
                _feedbackLightImage.color = color;
            }
        }

        private bool AreCombinationEqual(List<string> currentCombination, List<string> correctCombination)
        {
            if (currentCombination.Count != correctCombination.Count)
            {
                return false;
            }

            for (int i = 0; i < currentCombination.Count; i++)
            {
                if (currentCombination[i] != correctCombination[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
