﻿using UnityEngine;
using App.Controller.Inventory;

namespace App.View.Inventory
{
    [RequireComponent(typeof(CanvasGroup))]
    public class ShowOnItemInInventory : MonoBehaviour
    {
        [SerializeField]
        private InventoryItems _item = default;

        [SerializeField]
        private bool _invert = default;

        private InventoryController _inventoryController;
        private CanvasGroup _canvasGroup;

        private void Awake()
        {
            _inventoryController = GetComponentInParent<InventoryController>();
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        private void OnEnable()
        {
            _inventoryController.ItemsInInventory.CollectionChanged += OnItemsInInventoryChanged;
        }

        private void Start()
        {
            SetVisibility();
        }

        private void OnDisable()
        {
            _inventoryController.ItemsInInventory.CollectionChanged -= OnItemsInInventoryChanged;
        }

        private void OnItemsInInventoryChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            SetVisibility();
        }

        private void SetVisibility()
        {
            bool show = _inventoryController.ItemsInInventory.Contains(_item) ^ _invert;

            _canvasGroup.alpha = show ? 1f : 0f;
            _canvasGroup.blocksRaycasts = show;
        }
    }
}
