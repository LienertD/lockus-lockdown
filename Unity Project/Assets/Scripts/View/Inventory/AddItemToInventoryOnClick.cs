﻿using UnityEngine;
using UnityEngine.UI;
using App.Controller.Inventory;

namespace App.View.Inventory
{
    [RequireComponent(typeof(Button))]
    public class AddItemToInventoryOnClick : MonoBehaviour
    {
        [SerializeField]
        private InventoryItems _item = default;

        [SerializeField]
        private bool _removeFromInventoryWhenAlreadyIn = false;

        private Button _button;
        private InventoryController _inventoryController;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _inventoryController = GetComponentInParent<InventoryController>();
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(() => OnButtonClick());
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(() => OnButtonClick());
        }

        private void OnButtonClick()
        {
            // Add to inventory
            if (!_inventoryController.ItemsInInventory.Contains(_item))
            {
                _inventoryController.ItemsInInventory.Add(_item);
            }
            else
            {
                // Already in inventory
                if (_removeFromInventoryWhenAlreadyIn)
                {
                    _inventoryController.ItemsInInventory.Remove(_item);
                }
            }
        }
    }
}
